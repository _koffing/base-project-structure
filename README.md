# Starter structure 

This starter structure is used to kick off website development so that the build process and technologies used are similar across all projects. This document will go over the contents of the structure and how to use it.

### Quick start

To get this project up and running do the following:

1. In the terminal navigate to the root of the project which is where the package.json is located, then run **yarn** (or **npm install** if you do not use yarn).
2. Once the dependencies have finished installing, from the same directory in the terminal run **gulp**. The gulp command will build the project in the dis folder and start a node.js server on localhost:3000
3. If you do not want to run the dev server you can run **gulp build** to have the project built in dist with sourcemaps for js and css.
4. If you do not wish to have source maps in your build run **gulp prod** to have the project built in dist without sourcemaps.


### Package manager

The starter structure uses yarn for package management do to its ability to lock down dependencies so same dependencies will be installed the same exact way across every machine regardless of install order. You still have the option to use npm install but yarn is more likely to get the proper dependencies minimizing time spent debugging build processes. 


### Build process

The starter structure uses gulp for all of the build tasks in this project. The gulpfile.js that the task are written in is located in the root of the project. Each task in the gulpfile is commented with descriptions of what the task does. There are four main tasks that can be run to build the files or remove the build files. To run the gulp task you will need to open up a terminal in the directory that contains gulpfile.js The tasks are as follows.

- **gulp**: Run the **gulp** command to:
    - have the project built in the dist folder with source maps
    - Start a dev server on localhost:3000
    - watch: html, scss, and js files for changes, automatically run their associated tasks  and reload the browser.
- **gulp build**: Run the **gulp build** command to: 
    - have the project built in the dist folder with source maps
- **gulp prod**: Run the **gulp prod** command to:
    - have the project built in the dist folder without source maps
- **gulp clean**: Run the **gulp clean** command to:
    - Deletes the dist folder

###### Gulp notes:
When adding images or new files to the project: stop the dev server and re-run gulp to have gulp track your new files.


### Folder structure:
- **src**: is the first directory, it contains the two following directories: 
    - **assets**: the assets folder contains the following:
        - **css**: the css directory is where we place any css that is not supposed to get compiled into the main css file such as libraries or plugins, the files placed in this directory will be moved by gulp into dist/assets/css (this separation of files is needed for ie9 support)
        - **fonts**: the fonts folder is where font files are placed, the contents of this folder will be moved by gulp into dist/assets/fonts.
        - **imgs**: the imgs folder is where all image assets assets are placed, the contents of this folder will be moved by gulp into dist/assets/imgs.
        - **js**: In the js folder there is a main folder and a vendors folder.
            - **main**: The main folder is where custom javascript files are placed. These files will be mapped, preprocessed with bable, concatenated, minified,  renamed, and placed in **dist/assets/js/app.min.js** using gulp. **For these files to be concatenated the path to the file from the gulp file must be entered into the mainScripts array in the gulp file.**
            - **Vendors**: The vendors folder is where vendor libraries and plugins are placed. These files will be concatenated, minified, renamed, and placed in dist/assets/js/vendors.min.js using gulp.  **For these files to be concatenated the path to the file from the gulp file must be entered into the vendorScripts array in the gulp file.**
        - **scss**: the scss folder contains the scss file structure that will get mapped, preprocessed, autoprefixed, minified, renamed and placed in dist/assets/css/main.min.css.
- **Templates**: the templates file contains the twig templates that make up the views for the site. These files will be compiled by gulp into flat files and placed in the dist folder.


### Scss structure 

The file structure for scss as follows:

- **base**:  the base folder contains the default styles and is meant for styles that are only applied to element selectors.
- **layout**: the layout folder is for styling page sections. Layouts hold one or more modules together.
- **modules**: the modules folder is for styling modules. Modules are the reusable, modular parts of our design. They are the callouts, the sidebar sections, the product lists and so on.
- **Templates**: the templates folder is used to modify layout and modules on a page level. This involves placing a wrapper around the page and modifying layout and module style based on the wrapper class.
- **variables**: the variables folder is used for storing global Sass variables
- **vendors**: the vendors folder is a place or import to put sass that belongs to plugins. 
- **main.scss**: the main.scss file is where all of the aforementioned folder will be imported in the proper order: vendors, variables, base, modules, layout, templates. The folders are included in that order so that the styles of  modules, sections, or templates can more easily overwrite the styles of the elements, modules, or sections contained within.

### Css style methodology 

The style that the css is written in should closely resemble BEM (Block, Element, Modifier). For a good introduction to BEM I suggest heading over to http://getbem.com/introduction/  . 		I also suggest going to http://getbem.com/naming/  for an overview on the naming conventions.