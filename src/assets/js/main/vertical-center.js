/*!
 * vertical center code relies on the imagesLoaded PACKAGED v4.1.1 located in img-load.js
 */

(function($){
    verticalCenter();
    function verticalCenter() {
        var $centeredItem = $('[data-center]');

        function center_call(){
            $centeredItem.each(function(){
                var $item = $(this);
                $item.css({
                    "padding-top": "0",
                    "margin": "0"
                });
                var $item_img = $item.find('img');
                // checking if there is an img as a child of the centered item so center call can be delayed till after image loads.
                if($item_img.length){
                    $item.imagesLoaded(function() {
                        center($item);
                    });
                } else {
                    center($item);
                }
            });
        }

        // function used to centers the item
        function center($item){
            var ItemHeight = $item.outerHeight();
            var parentH = $item.parent().outerHeight();
            var newPadding = (parentH - ItemHeight)/2;
            var breakpoint = 0;
            var centerOff = true;
            var activeMobile = $item.attr('data-center');

            switch(activeMobile) {
                case 'xs':
                    breakpoint = 0;
                    break;
                case 'sm':
                    breakpoint = 769;
                    break;
                case 'md':
                    breakpoint = 992;
                    break;
                case 'lg':
                    breakpoint = 1200;
                    break;
                case 'true':
                    centerOff = true;
                    break;
                case 'false':
                    centerOff = false;
                    break;
                default: breakpoint = 0;
            }

            if (newPadding <= 0){
                newPadding = 0;
            }
            if($(window).width() >= breakpoint && centerOff == true){
                $item.css({
                    "padding-top": newPadding,
                    "margin": "0"
                });
            } else {
                $item.css({
                    "padding-top": "0",
                    "margin": "0"
                });
            }
        }

        center_call();

        //checking if the resize event is done
        var rtime;
        var resizeListener = true;

        function resizeCheck(time){
            var setTime;
            var compTime;
            var timer1;
            var timer2;
            timer1 = setInterval(function(){
                setTime = rtime;
                timer2 = setTimeout(function(){ compTime = rtime},100);
                    if(setTime == compTime) {
                        center_call();
                        clearInterval(timer2);
                        clearInterval(timer1);
                        resizeListener = true;
                    }
                },200);
        }

        $(window).on('resize', function(){
            rtime = new Date().getTime();
            if (resizeListener){
                resizeCheck(rtime);
            }
            resizeListener = false;
        });
    }

})(jQuery);