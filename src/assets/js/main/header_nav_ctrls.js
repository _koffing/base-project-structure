(function($){
    /*
    Adding Swipe events to jquery.
    */
    $.fn.menuCtrlSwipeRight = function(cb){;
    return this.each(function(){
        swipe(this, 'right', cb);
    });
    }
    $.fn.menuCtrlSwipeLeft = function(cb){
        return this.each(function(){
            swipe(this, 'left', cb);
        });
    }


    function swipe(el, direction, cb){
        var element = el;
        var swipebool;
            var startP;
            var startX;
            var startY;
            var directionModifier;
            var swipeDirection;
            switch(direction){
                case 'right':
                    directionModifier = -1;
                    swipeDirection = true;
                    break;
                case 'left':
                    directionModifier = 1;
                    swipeDirection = true;
                    break;
                default: directionModifier = 1;;
            }
            element.addEventListener("touchstart", function(e){
                swipebool = true;
                startX = e.changedTouches[0].clientX;
                startY = e.changedTouches[0].clientY;
            }, false); 
            element.addEventListener("touchmove", function(e){
                var distanceX = startX - e.changedTouches[0].clientX;
                var distanceY = startY - e.changedTouches[0].clientY;
                if(swipeDirection){
                    if(distanceY < 0 ){distanceY *= -1;}
                    if (distanceX <= 150 && swipebool && distanceY < (distanceX * directionModifier)){
                        swipebool = false;
                        return cb.call(this, el);
                    }
                } else {
                    if(distanceX < 0 ){distanceX *= -1;}
                    if (distanceY <= 150 && swipebool && distanceX < (distanceY * directionModifier)){
                        swipebool = false;
                        return cb.call(this, el);
                    }
                }
                
            }, false); 
    }//end jquery swipe events

    /*
    Menu Controler object
    */

    function MenuCtrl(menu, options) {
        this.Menu = $(menu);
        this.adminBar = true;
        this.breakpoint = 1200;
        this.transition = 400;
        this.activeToggle_1;
        this.activeMenu_1;
        this.activeToggle_2;
        this.activeMenu_2;  
        this.activeTogglelist = [];
        return this.init(options);
    }

    MenuCtrl.prototype.init = function(options) {
        var self = this;
        this.setOptions(options);
        this.setClasses();
        this.setHoverEvents();
        this.setToggle_1();
        this.setToggle_2();
        this.mobileNavToggle();
        this.overlayEvents();
        this.setResize();
    };

    MenuCtrl.prototype.setOptions = function(options) {
        if(options.breakpoint){
            this.breakpoint = options.breakpoint;
        }
        if(options.transition){
            this.transition = options.transition;
        }
    };


    MenuCtrl.prototype.setClasses = function(menu) {
        var self = this;
        self.Menu.children('.menu-item-has-children').addClass('nav-toggle-1');
        self.Menu.children('.menu-item-has-children').children('.sub-menu').children('.menu-item-has-children').addClass('nav-toggle-2');
    };


    MenuCtrl.prototype.setHoverEvents = function() {
        var self = this;
        var $hoverItem;
        var $submenu;
        $('.nav-toggle-1').hover(function(e){
            if(window.innerWidth >= self.breakpoint){
                $hoverItem = $(this);
                $submenu = $hoverItem.children(".sub-menu");
                $submenu.stop().slideDown(self.transition);
            }
        }, function(){
            if(window.innerWidth >= self.breakpoint){
                $submenu.stop().slideUp(self.transition);
                self.closeSubMenus_2();
            }
        });
    };


    MenuCtrl.prototype.setToggle_1 = function() {
        var self = this;
        $('.nav-toggle-1 > a').on('click touchstart', function(e) {
			e.preventDefault();
            var $this = $(this);
            if(!$this.hasClass('active')){
                self.closeSubMenus_1();
                self.closeSubMenus_2();
                self.activeToggle_1 = $this;
                self.activeMenu_1 = $this.siblings(".sub-menu");
                self.activeToggle_1.addClass('active');
                self.activeMenu_1.stop().slideDown(self.transition);
            } else {
                self.closeSubMenus_2();
                self.activeToggle_1.removeClass('active');
                self.activeMenu_1.stop().slideUp(self.transition);
            }
        });
    };


    MenuCtrl.prototype.setToggle_2 = function() {
        var self = this;
        $('.nav-toggle-2 > a').on('click', function(e) {
			e.preventDefault();
             var $this = $(this);
             $this.parents('ul').css({"height": "auto"});
            if(!$this.hasClass('active')){
                if(window.innerWidth < self.breakpoint){
                    self.closeSubMenus_2();
                }
                self.activeToggle_2 = $this;
                self.activeMenu_2 = $this.siblings(".sub-menu");
                self.activeToggle_2.addClass('active');
                self.activeMenu_2.stop().slideDown(self.transition);
                self.activeTogglelist.push({toggle: self.activeToggle_2, menu: self.activeMenu_2});
            } else {
                $this.removeClass('active');
                $this.siblings('.sub-menu').stop().slideUp(self.transition);
            }
        });
    };

    MenuCtrl.prototype.closeSubMenus_1 = function() {
        var self = this;
        if(self.activeToggle_1 != undefined){
            self.activeToggle_1.removeClass('active');
            self.activeMenu_1.stop().slideUp(self.transition, function(){
                    $(this).css({"height": "auto"});
                });
            self.activeToggle_1 = undefined;
        }
    };

    MenuCtrl.prototype.closeSubMenus_2 = function() {
        var self = this;
        if(self.activeTogglelist != undefined){
            self.activeTogglelist.forEach(function(item){
                item.toggle.removeClass('active');
                item.menu.stop().slideUp(self.transition);
                self.activeToggleList = [];
            });    
        }
    };


    MenuCtrl.prototype.mobileNavToggle = function(){
        var self = this;
        var $body = $('body');
        var $header = $('.header');
        $('.nav-toggle').on('click', function(){
            $body.addClass('menu-active');
            self.toggleOverlay();
        });
        $('.nav-close-toggle').on('click', function(){
            $body.removeClass('menu-active');
            self.closeSubMenus_2();
            self.closeSubMenus_1();
            self.toggleOverlay();
        });
        
    }

     MenuCtrl.prototype.toggleOverlay = function(){
        var self = this;
        var $body = $('body');
        var $header = $('.header');
        if(!$body.hasClass('active-overlay')){
            $header.append("<div class='nav-overlay'></div>");
              setTimeout(function(){
                $body.addClass('active-overlay');
            },50);
        }else {   
            $body.removeClass('active-overlay');
            setTimeout(function(){
                $(".nav-overlay").remove();
            },self.transition + 100);
        }
    }


    MenuCtrl.prototype.overlayEvents = function(){
        var self = this;
        var $body = $('body');
        $(document).on('click', '.nav-overlay', function(){
            $body.removeClass('menu-active');
            self.closeSubMenus_2();
            self.closeSubMenus_1();
            self.toggleOverlay();
        });
        $('.main-nav').menuCtrlSwipeRight(function(){
            $body.removeClass('menu-active');
            self.closeSubMenus_2();
            self.closeSubMenus_1(); 
            self.toggleOverlay();
        });
    }

     MenuCtrl.prototype.setResize = function() {
         var self = this;
        $(window).on('resize', function(){
            $('body').removeClass('menu-active');
            self.closeSubMenus_1();
            self.closeSubMenus_2();
        });
    };

    var MainMenu = new MenuCtrl('#menu', {
        breakpoint: 1024,
        transition: 200,
        adminbarCheck: true
    });

})(jQuery);



